﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class AnimTestGameMode : GameMode
{
    private string loadedText;

    public AnimTestGameMode() : base(false, false, false, false, false, true, true, false, false, false, false, false, false, 1)
    {
        loadedText = "";        
    }
    private void loadTextIntoString()
    {        
        loadedText = new string('a', 1000);
    }

    public override string GetGameModeTextContent()
    {
        if (loadedText.Length == 0)
        {
            loadTextIntoString();
        }

        return loadedText;
    }

    public override char[] GetTextChars(int startIndex, int length)
    {
        return GetGameModeTextContent().ToUpper().ToCharArray(startIndex, length);
    }

    public override int GetGameModeTextLength()
    {
        if (loadedText.Length == 0)
        {
            loadTextIntoString();
        }
        return loadedText.Length;
    }
}

