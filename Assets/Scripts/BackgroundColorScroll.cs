﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundColorScroll : MonoBehaviour
{
    private SpriteRenderer _thisSprite;
    public Gradient strobeColors;
    public float strobeDuration = 3f;


    // Start is called before the first frame update
    void Start()
    {
        _thisSprite = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        float currentTime = Mathf.PingPong((Time.time / strobeDuration), 1f);
        _thisSprite.color = strobeColors.Evaluate(currentTime);
    }
}
