using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class BackgroundMusicManager : MonoBehaviour
{
    public List<AudioClip> musicLibrary = new List<AudioClip>();

    private AudioSource _audioSource;
    private AudioClip _currentAudioClip;    
    private bool _pauseAudio = true;


    // Start is called before the first frame update
    void Start()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();              
    }

    // Start the BGM queue
    public void StartAudioQueue(int initialSong, bool playOnStart = true)
    {
        QueueNextAudio(initialSong);
        _pauseAudio = !playOnStart;
        PlayCurrentAudio();
    }

    public void StartAudioQueueAndPlay(int initialSong)
    {
        StartAudioQueue(initialSong, true);
    }

    // Set/Change the next audio to play
    public void QueueNextAudio(int songIndex)
    {        
        if (musicLibrary[songIndex] != null)
        {
            _currentAudioClip = musicLibrary[songIndex];
        }
        
    }

    // Stop the audio but keep scanning for changes
    public void StopAudioNow()
    {
        _audioSource.Stop();
        _pauseAudio = true;
        CancelInvoke("PlayCurrentAudio");
        Invoke("PlayCurrentAudio", 0.1f);
    }

    // Stop the audio and instantly play the next track
    public void StopCurrentAudioAndPlay(int songIndex)
    {
        _audioSource.Stop();
        CancelInvoke("PlayCurrentAudio");
        StartAudioQueueAndPlay(songIndex);
    }

    // Stop the audio and the cancel the invoke
    public void StopCurrentAudioForGood()
    {
        _audioSource.Stop();
        CancelInvoke("PlayCurrentAudio");
    }

    public void PlayCurrentAudio()
    {
        if (!_pauseAudio)
        {
            _audioSource.clip = _currentAudioClip;
            _audioSource.Play();
            Invoke("PlayCurrentAudio", _audioSource.clip.length);
        }
        else
        {
            _audioSource.Stop();
            Invoke("PlayCurrentAudio", 0.1f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
