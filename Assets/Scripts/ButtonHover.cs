﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent (typeof(Button))]
public class ButtonHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    private Text _buttonText;
    private Color _baseColor;
    private Color _highlightColor = new Color(0f, 1f, 0f, 1f);
    private Color _deactiveColor = new Color(0.2f, 0.2f, 0.2f, 05f);
    private Button _thisButton;
    private string _defaultButtonText;
    private bool _interactionDelay;
    
    // Start is called before the first frame update
    private void Start() {
        _buttonText = GetComponentInChildren<Text>();
        _baseColor = _buttonText.color;
        _thisButton = gameObject.GetComponent<Button>();
        _interactionDelay = _thisButton.interactable;
        _defaultButtonText = _buttonText.text;
    }

    // Update is called once per frame
    private void Update() {
        if (_thisButton.interactable == _interactionDelay) {
            if (_thisButton.interactable) {
                //_buttonText.color = _baseColor;
            } else {
                _buttonText.color = _deactiveColor;
            }
        }
        _interactionDelay = _thisButton.interactable;
    }

    public void OnPointerEnter(PointerEventData event_data) {
        if (_thisButton.interactable)
        {
            _buttonText.color = _highlightColor;
            _buttonText.text = "> " + _defaultButtonText;
        }
        else
        {
            _buttonText.color = _deactiveColor;
        }
    }

    public void OnPointerExit(PointerEventData event_data) {
        if (_thisButton.interactable)
        {
            _buttonText.color = _baseColor;
            _buttonText.text = _defaultButtonText;
        }
        else
        {
            _buttonText.color = _deactiveColor;
        }
    }

    public void OnPointerDown(PointerEventData event_data) {
        if (_thisButton.interactable)
        {
            _buttonText.color = _highlightColor;
            _buttonText.text = "> " + _defaultButtonText;
        }
        else
        {
            _buttonText.color = _deactiveColor;
        }
    }

    public void OnPointerUp(PointerEventData event_data) {
        if (_thisButton.interactable)
        {
            _buttonText.color = _highlightColor;
            _buttonText.text = "> " + _defaultButtonText;
        }
        else
        {
            _buttonText.color = _deactiveColor;
        }
    }
}
