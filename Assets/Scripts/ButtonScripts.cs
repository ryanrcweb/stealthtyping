using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Audio;

public class ButtonScripts : MonoBehaviour
{    
    public GameObject controlsGo;
    public GameObject optionsGo;
    public GameObject newGameOptionsGo;
    public GameObject loadingBarsGo;
    public GameObject menuGo;
    public AudioMixer masterMixer;

    public void ShowPanel(GameObject targetPanel)
    {
        targetPanel.SetActive(true);
    }

    public void HidePanel(GameObject targetPanel)
    {
        targetPanel.SetActive(false);
    }

    public void OpenOptionsPanel()
    {
        if (!optionsGo.activeSelf)
        {
            optionsGo.SetActive(true);
            controlsGo.SetActive(false);

            float currentMasterVol = PlayerPrefs.GetFloat("currentMasterVol");
            GameObject masterVolSliderGo = GameObject.Find("options_master_vol_slider");

            float currentMusicVol = PlayerPrefs.GetFloat("currentMusicVol");
            GameObject musicVolSliderGo = GameObject.Find("options_music_vol_slider");

            float currentSFXVol = PlayerPrefs.GetFloat("currentSFXVol");
            GameObject SFXVolSliderGo = GameObject.Find("options_sfx_vol_slider");

            masterVolSliderGo.GetComponent<Slider>().value = currentMasterVol;
            musicVolSliderGo.GetComponent<Slider>().value = currentMusicVol;
            SFXVolSliderGo.GetComponent<Slider>().value = currentSFXVol;
        }        
    }    

    public void CloseOptionsPanel()
    {
        if (optionsGo.activeSelf)
        {
            GameObject masterVolSliderGo = GameObject.Find("options_master_vol_slider");
            GameObject musicVolSliderGo = GameObject.Find("options_music_vol_slider");
            GameObject SFXVolSliderGo = GameObject.Find("options_sfx_vol_slider");

            PlayerPrefs.SetFloat("currentMasterVol", masterVolSliderGo.GetComponent<Slider>().value);
            PlayerPrefs.SetFloat("currentMusicVol", musicVolSliderGo.GetComponent<Slider>().value);
            PlayerPrefs.SetFloat("currentSFXVol", SFXVolSliderGo.GetComponent<Slider>().value);

            optionsGo.SetActive(false);
            controlsGo.SetActive(true);
        }
    }

    public void SetMasterVolume(float value)
    {
        masterMixer.SetFloat("masterVol", SharedMethods.ConvertToDecibels(value));
        PlayerPrefs.SetFloat("currentMasterVol", value);
    }

    public void SetMusicVolume(float value)
    {
        masterMixer.SetFloat("musicVol", SharedMethods.ConvertToDecibels(value));
        PlayerPrefs.SetFloat("currentMusicVol", value);
    }

    public void SetSFXVolume(float value)
    {
        masterMixer.SetFloat("sfxVol", SharedMethods.ConvertToDecibels(value));
        PlayerPrefs.SetFloat("currentSFXVol", value);
    }

    public void OpenNewGamePanel()
    {
        if (!newGameOptionsGo.activeSelf)
        {
            newGameOptionsGo.SetActive(true);
            controlsGo.SetActive(false);

            InputField seedInputGo = GameObject.Find("new_game_wpm_text_input").GetComponent<InputField>();
            seedInputGo.Select();
            seedInputGo.ActivateInputField();
        }
    }

    public void CloseNewGamePanel()
    {
        if (newGameOptionsGo.activeSelf)
        {
            GameGlobals.Instance.currentSeed = null;
            newGameOptionsGo.SetActive(false);
            controlsGo.SetActive(true);
        }
    }

    public void StartGame(int mode)
    {
        if (GameObject.Find("new_game_wpm_text_input") != null)
        {
            GameGlobals.Instance.currentSeed = GameObject.Find("new_game_wpm_text_input").GetComponent<InputField>().text;
        }
        else
        {
            if (mode != 0)
            {
                GameGlobals.Instance.forceLoadScene = mode;
            }
        }

        loadingBarsGo.SetActive(true);
        menuGo.SetActive(false);
        
        Invoke(nameof(FauxLoadEnd), 2f);
    }

    // Faux load invoke allows the loadbars to animate a little,
    // and show a message related to the seed (to do)
    private void FauxLoadEnd()
    {
        GameGlobals.Instance.ParseSeedWink();
    }

    public void BackToMainMenu()
    {
        GameGlobals.Instance.LoadSelectedLevel(GameModeScenes.MainMenu);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
