using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboTextManager : MonoBehaviour
{
    public Gradient comboColors;
    public Text comboTextBox;

    private bool _comboActive = false;
    private float _comboEndFadeTime = 6f;
    private float _currentFadeTime = 0f;
    private float _subFadeTime = 0f;

    public void UpdateComboText(int currentCombo, bool comboEnded, uint comboScore = 0)
    {
        if (comboEnded)
        {
            comboTextBox.text = currentCombo.ToString() + " COMBO (+" + comboScore.ToString() + ")";
            _comboActive = false;
            _currentFadeTime = 0f;
        }
        else
        {
            comboTextBox.text = "COMBO X " + currentCombo.ToString();
            int comboPerc = currentCombo / 25;
            _comboActive = true;
            _currentFadeTime = 0f;
            if (comboPerc >= 0 && comboPerc <= 1)
            {
                comboTextBox.color = comboColors.Evaluate(0f);
                int sizeInc = (1 + (currentCombo % 26));
                if (sizeInc > 4)
                { 
                    sizeInc = 4;
                }                
                comboTextBox.fontSize = 40 + sizeInc;
            }
            else if (comboPerc > 1 && comboPerc <= 2)
            {
                comboTextBox.color = comboColors.Evaluate(0.25f);
                comboTextBox.fontSize = 45 + (1 + ((currentCombo - 25) % 6));
            }
            else if (comboPerc > 2 && comboPerc <= 3)
            {
                comboTextBox.color = comboColors.Evaluate(0.50f);
                comboTextBox.fontSize = 50 + (1 + ((currentCombo - 30) % 6));
            }
            else if (comboPerc > 4)
            {
                int sizeInc = 0;
                comboTextBox.color = comboColors.Evaluate(1f);
                if ((currentCombo - 35) >= 35)
                {
                    sizeInc = 15;
                }
                else
                {
                    sizeInc = (1 + ((currentCombo - 35) % 16));
                    if (sizeInc > 15)
                    {
                        sizeInc = 15;
                    }
                }
                comboTextBox.fontSize = 55 + sizeInc;
            }
        }        
    }

    // Update is called once per frame
    void Update()
    {
        if (!_comboActive)
        {
            if (_currentFadeTime <= _comboEndFadeTime)
            {
                if (_subFadeTime > 0.1f)
                {
                    Color currentTextColor = comboTextBox.color;
                    currentTextColor.a -= (1 / _comboEndFadeTime);
                    comboTextBox.color = currentTextColor;
                    _currentFadeTime += Time.deltaTime;
                    _subFadeTime = 0f;
                }
                else
                {
                    _subFadeTime += Time.deltaTime;
                }
            }
            else
            {
                Color currentTextColor = comboTextBox.color;
                if (currentTextColor.a > 0f)
                {
                    currentTextColor.a = 0f;
                    comboTextBox.color = currentTextColor;
                }
            }
        }
    }
}
