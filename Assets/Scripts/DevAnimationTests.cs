using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DevAnimationTests : MonoBehaviour
{
    private GameObject _playerGO;
    private GameObject _groundGO;
    private GameObject _inputSpeed;
    private GameObject _inputAnim;

    private string _currentPlayerAnimation;
    private float _currentGroundSpeed;
    private float _currentGroundOffset = 0f;

    // Start is called before the first frame update
    void Start()
    {
        _playerGO = GameObject.Find("player");
        _groundGO = GameObject.Find("ground_bg");
        _inputAnim = GameObject.Find("player_animation_select");
        _inputSpeed = GameObject.Find("background_scroll_speed");
    }

    // Load the currently selected settings to the dev scene
    public void DoCurrentSettings()
    {
        string speedInput = _inputSpeed.GetComponent<InputField>().text;        
        if (float.TryParse(speedInput, out float speedFloat))
        {
            _currentGroundSpeed = speedFloat;
        }
        _currentPlayerAnimation = _inputAnim.GetComponent<Dropdown>().options[_inputAnim.GetComponent<Dropdown>().value].text;
        _playerGO.GetComponent<PlayerAnims>().ForcePlayerAnimation(_currentPlayerAnimation);
        Debug.Log("did it");
    }

    // Update is called once per frame
    void Update()
    {
        //_currentGroundOffset += _currentGroundSpeed;
        _groundGO.GetComponent<GroundAnim>().CinematicMoveGround(_currentGroundSpeed);
    }
}
