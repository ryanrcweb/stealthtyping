﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class DevGameMode : GameMode
{
    public DevGameMode() : base()
    {
        // Everything is enabled
        _loadedText = "";

        _missionTimeCap = 180f;

        List<string> missionIntroLines = new List<string>();
        missionIntroLines.Add("...Bzzzt...");
        missionIntroLines.Add("H...Hell....o?....Found loc....");
        missionIntroLines.Add("...on....way.....e.t.a....");
        missionIntroLines.Add("....Keep running!....");

        List<string> missionEndingLinesA = new List<string>();
        missionEndingLinesA.Add("We're almost there!");
        missionEndingLinesA.Add("Keep going, we wont be able to land.");
        missionEndingLinesA.Add("E.T.A in 10 seconds.");

        List<string> missionEndingLinesB = new List<string>();        
        missionEndingLinesB.Add("..5..");
        missionEndingLinesB.Add("..4..");
        missionEndingLinesB.Add("..3..");
        missionEndingLinesB.Add("..2..");
        missionEndingLinesB.Add("..1..");

        List<string> missionOutcomeLinesA = new List<string>();
        missionOutcomeLinesA.Add("Good job, here we come.");
        missionOutcomeLinesA.Add("We're headed down to extract you.");

        List<string> missionOutcomeLinesB = new List<string>();
        missionOutcomeLinesB.Add("Oi! Lazy bugger, move your arse.");
        missionOutcomeLinesB.Add("Get a running start...");
        missionOutcomeLinesB.Add("We're headed down to extract you.");

        _dialogLibrary.Add("intro", missionIntroLines);
        _dialogLibrary.Add("timeOutA", missionEndingLinesA);
        _dialogLibrary.Add("timeOutB", missionEndingLinesB);
        _dialogLibrary.Add("outcomeA", missionOutcomeLinesA);
        _dialogLibrary.Add("outcomeB", missionOutcomeLinesB);
    }

    private void loadTextIntoString()
    {        
        TextAsset tempText = Resources.Load<TextAsset>("the_looking_glass");
        _loadedText = "  " + tempText.text;
    }

    public override string GetGameModeTextContent()
    {
        if (_loadedText.Length == 0)
        {
            loadTextIntoString();
        }

        return _loadedText;
    }

    public override char[] GetTextChars(int startIndex, int length)
    {
        return GetGameModeTextContent().ToUpper().ToCharArray(startIndex, length);
    }

    public override int GetGameModeTextLength()
    {
        if (_loadedText.Length == 0)
        {
            loadTextIntoString();
        }
        return _loadedText.Length;
    }        
}


