using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class DialogTextWriter : MonoBehaviour
{
    private TypingEngine _gameEngine;
    private GameMode _currentGameMode;

    private string _currentString = "";
    private float _timeStep = 0.05f;
    private float _currentTimeStep = 0f;
    private float _currentLineTimeStep = 0f;
    private int _currentLetter = 0;
    private int _currentStringIndex = 0;

    private string _currentSection = "";
    private float _currentSectionLength;
    private float _dialogStepLength = 0f;

    private AudioSource _attachedAudioSource;

    public Text currentTextbox;
    //public List<string> dialogList = new List<string>();

    public void SetupCurrentSection(string currentSection)
    {
        _currentSection = currentSection;
        _currentString = "";
        _currentLetter = 0;
    }

    // Setting up timers for the dialogs, this allows for adding or removing custom lines
    // without having to recreate a timeline for it
    public void SetupCurrentTimeframeLength(float timeGiven)
    {
        int totalNumberOfLines = _currentGameMode.GetDialogLineCountFromLibrary(_currentSection);

        if (totalNumberOfLines > 0)
        {
            _currentSectionLength = timeGiven;
            _dialogStepLength = timeGiven / (float) totalNumberOfLines;
        }
    }

    public void StartCurrentDialog()
    {
        _currentLetter = 0;
        _currentTimeStep = 0f;
        _currentStringIndex = 0;
        _currentLineTimeStep = 0f;
        _currentString = _currentGameMode.GetDialogLineFromLibrary(_currentSection, _currentStringIndex);
        currentTextbox.text = "";
    }

    // Start is called before the first frame update
    void Awake()
    {
        //dialogList = new List<string>();
        _attachedAudioSource = gameObject.GetComponent<AudioSource>();
        _gameEngine = GameObject.Find("game_engine").GetComponent<TypingEngine>();
        _currentGameMode = _gameEngine.currentGameModeRules;
    }

    // Update is called once per frame
    void Update()
    {
        if (_currentSection.Length > 0)
        {
            if (_currentString.Length > 0 && _currentLetter <= _currentString.Length)
            {
                _currentTimeStep += Time.deltaTime;
                if (_currentTimeStep >= _timeStep)
                {
                    currentTextbox.text = _currentString.Substring(0, _currentLetter);
                    if (_currentLetter < _currentString.Length)
                    {
                        currentTextbox.text += "<color=#00000000>" + _currentString.Substring(_currentLetter) + "</color>";
                    }

                    _currentLetter++;
                    _currentTimeStep = 0f;
                    _attachedAudioSource.Play();
                }
            }
            else
            {
                if (_currentString.Length > 0 && _currentLetter >= _currentString.Length)
                {

                }
            }

            if (_currentLineTimeStep >= _dialogStepLength)
            {
                if (_currentSection.Length > 0 && _currentStringIndex < _currentGameMode.GetDialogLineCountFromLibrary(_currentSection))
                {
                    _currentLetter = 0;
                    _currentTimeStep = 0f;
                    _currentStringIndex++;
                    string currentLineString = _currentGameMode.GetDialogLineFromLibrary(_currentSection, _currentStringIndex);
                    if (currentLineString != null)
                    {
                        _currentString = currentLineString;
                    }
                    else
                    {
                        _currentString = "";
                    }

                    currentTextbox.text = "";
                }
                _currentLineTimeStep = 0f;
            }
            else
            {
                _currentLineTimeStep += Time.deltaTime;
            }
        }
    }
}
