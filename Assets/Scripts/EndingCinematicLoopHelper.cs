using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


public class EndingCinematicLoopHelper : MonoBehaviour
{
    private PlayableDirector _currentDirector = null;
    private double _loopStartTime;

    // Set the start timecode for the looped section
    public void SetLoopStartTime()
    {
        if (_currentDirector == null)
        {
            _currentDirector = gameObject.GetComponent<PlayableDirector>();
        }

        _loopStartTime = _currentDirector.time;
    }

    // Go back to the loop start position
    public void GoBack()
    {
        if (_currentDirector == null)
        {
            _currentDirector = gameObject.GetComponent<PlayableDirector>();
        }

        _currentDirector.time = _loopStartTime;
        
        

    }

}
