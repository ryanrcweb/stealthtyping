using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Enum of possible scene index numbers
public enum GameModeScenes
{
    Preload = 0,
    MainMenu = 1,
    WPMTest = 2,
    AnimDev = 3,
    LevelEdit = 4
}

public class GameGlobals : MonoBehaviour
{
    // Create a singleton static to hold the global settings for the game
    private static GameGlobals _instance;
    public static GameGlobals Instance
    {
        get
        {           
            return _instance;
        }
    }


    // Seed Vars
    public string currentSeed;
    public int forceLoadScene = -1;
    public bool specialSeed = false;
    public string seedDescription = null;
    public bool overrideMissionTime = false;
    public float overrideMissionTimeValue = 0f;
    public bool overrideMissionGameMode = false;
    public int overrideMissionGameModeValue = 0;

    // Parse the seed string given and set special globals if needed    
    public void ParseSeedWink()
    {
        specialSeed = false;        
        seedDescription = null;
        overrideMissionTime = false;
        overrideMissionTimeValue = 0f;

        if (forceLoadScene != -1)
        {
            int sceneToLoad = forceLoadScene;
            forceLoadScene = -1;

            LoadSelectedLevel((GameModeScenes)sceneToLoad);            
            return;
        }

        if (currentSeed.Length == 0)
        {
            currentSeed = Random.Range(0, 9999999).ToString();
        }

        switch (currentSeed.ToUpper())
        {
            case "LETSCTHOSEMOVES":
                specialSeed = true;
                seedDescription = "This had better be me...";
                LoadSelectedLevel(GameModeScenes.AnimDev);
                break;
            case "ILOVETYPING":
                specialSeed = true;
                seedDescription = "Longer mission timer mode, you must really love typing!";
                overrideMissionTime = true;
                overrideMissionTimeValue = 300f;
                LoadSelectedLevel(GameModeScenes.WPMTest);
                break;
            case "HAYAKU":
                specialSeed = true;
                seedDescription = "Shorter mission timer mode";
                overrideMissionTime = true;
                overrideMissionTimeValue = 60f;
                LoadSelectedLevel(GameModeScenes.WPMTest);
                break;
            default:                
                LoadSelectedLevel(GameModeScenes.WPMTest);
                break;
        }        
    }

    public void LoadSelectedLevel(GameModeScenes levelId)
    {
        SceneManager.LoadSceneAsync((int)levelId);
    }

    // Start is called before the first frame update
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        
    }
}
