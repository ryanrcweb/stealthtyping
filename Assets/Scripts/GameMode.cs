﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Gamemodes will hold vars for en/disabling parts of the game
// also methods for dealing with typing text generation/obstacle and enemy placement
public class GameMode
{
    private bool _gameModeHasScore;
    private bool _gameModeHasWPM;
    private bool _gameModeHasDistance;
    private bool _gameModeHasSoundLevel;
    private bool _gameModeHasAccuracy;
    private bool _gameModeHasLetters;
    private bool _gameModeHasPauseMenu;
    private bool _gameModeHasPlayer;
    private bool _gameModeHasCharacterHighlighter;
    private bool _gameModeHasTimer;
    private bool _gameModeHasGround;
    private bool _gameModeHasObstacles;
    private bool _gameModeHasCinematics;
    private int _initGameState;

    protected string _loadedText = "";
    protected float _missionTimeCap = 300f;
    protected Dictionary<string, List<string>> _dialogLibrary = new Dictionary<string, List<string>>();

    public GameMode()
    {
        _gameModeHasScore = true;
        _gameModeHasWPM = true;
        _gameModeHasDistance = true;
        _gameModeHasSoundLevel = true;
        _gameModeHasAccuracy = true;
        _gameModeHasLetters = true;
        _gameModeHasPauseMenu = true;
        _gameModeHasPlayer = true;
        _gameModeHasCharacterHighlighter = true;
        _gameModeHasTimer = true;
        _gameModeHasGround = true;
        _gameModeHasObstacles = true;
        _gameModeHasCinematics = true;
        _initGameState = -1;
    }

    public GameMode(bool score, bool wpm, bool distance, bool soundlevel, bool accuracy, bool letters, bool pausemenu, bool player, bool characterHighlighter, bool timer, bool ground, bool obstacles, bool cinematics, int initGameState)
    {
        _gameModeHasScore = score;
        _gameModeHasWPM = wpm;
        _gameModeHasDistance = distance;
        _gameModeHasSoundLevel = soundlevel;
        _gameModeHasAccuracy = accuracy;
        _gameModeHasLetters = letters;
        _gameModeHasPauseMenu = pausemenu;
        _gameModeHasPlayer = player;
        _gameModeHasCharacterHighlighter = characterHighlighter;
        _gameModeHasTimer = timer;
        _gameModeHasGround = ground;
        _gameModeHasObstacles = obstacles;
        _gameModeHasCinematics = cinematics;
        _initGameState = initGameState;
    }

    virtual public string GetGameModeTextContent()
    {
        string workingString = "";

        return workingString;
    }

    virtual public char[] GetTextChars(int startIndex, int length)
    {
        return null;
    }

    virtual public int GetGameModeTextLength()
    {
        return 0;
    }
        
    virtual public int GetDialogLineCountFromLibrary(string sectionName)
    {
        int lineCount = 0;

        if (_dialogLibrary.TryGetValue(sectionName, out List<string> currentSection))
        {
            lineCount = currentSection.Count;
        }

        return lineCount;
    }

    virtual public string GetDialogLineFromLibrary(string sectionName, int lineNumber)
    {
        string currentLine = null;        

        if (_dialogLibrary.TryGetValue(sectionName, out List<string> currentSection))
        {
            if ((currentSection.Count) > lineNumber)
            {
                if (currentSection[lineNumber].Length > 0)
                {
                    currentLine = currentSection[lineNumber];
                }
            }
        }

        return currentLine;
    }

    virtual public float GetGroundSpeedFromPlayerSpeed(float playerSpeed)
    {
        float resultOut;
        switch (playerSpeed)
        {
            case 1:
                resultOut = 0.5f;
                break;
            case 2:
                resultOut = 1f;
                break;
            case 3:
                resultOut = 2.5f;
                break;
            case 4:
                resultOut = 5f;
                break;
            case 0:
            default:
                resultOut = 0f;
                break;
        }

        return resultOut;
    }

    virtual public void MakeGameModeChangesToEngineInit(TypingEngine currentEngine)
    {
        
    }

    public bool GameModeHasScore 
    {
        get
        {
            return _gameModeHasScore;
        }
    }

    public bool GameModeHasWPM
    {
        get
        {
            return _gameModeHasWPM;
        }
    }

    public bool GameModeHasDistance 
    {
        get
        {
            return _gameModeHasDistance;
        }
    }
    public bool GameModeHasSoundLevel 
    { 
        get 
        { 
            return _gameModeHasSoundLevel; 
        } 
    }

    public bool GameModeHasAccuracy 
    { 
        get 
        { 
            return _gameModeHasAccuracy; 
        } 
    }

    public bool GameModeHasLetters 
    { 
        get 
        { 
            return _gameModeHasLetters; 
        } 
    }

    public bool GameModeHasPauseMenu 
    { 
        get 
        { 
            return _gameModeHasPauseMenu; 
        } 
    }

    public bool GameModeHasPlayer 
    { 
        get 
        { 
            return _gameModeHasPlayer; 
        } 
    }

    public bool GameModeHasCharacterHighlighter 
    { 
        get 
        { 
            return _gameModeHasCharacterHighlighter; 
        } 
    }

    public bool GameModeHasTimer 
    { 
        get 
        { 
            return _gameModeHasTimer; 
        } 
    }

    public bool GameModeHasGround 
    { 
        get 
        { 
            return _gameModeHasGround; 
        } 
    }

    public bool GameModeHasObstacles 
    { 
        get 
        { 
            return _gameModeHasObstacles; 
        } 
    }

    public bool GameModeHasCinematics
    {
        get
        {
            return _gameModeHasCinematics;
        }
    }

    public int InitGameState
    {
        get
        {
            return _initGameState;
        }
    }

    public float MissionTimeCap
    {
        get
        {
            return _missionTimeCap;
        }
    }
}

