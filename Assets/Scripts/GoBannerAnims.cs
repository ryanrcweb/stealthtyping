using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GoBannerAnims : MonoBehaviour
{
    private float _currentTime = 0f;
    private float _fadeOutTime = 0.1f;
    private float _fadeOutAmount = 0.05f;
    private bool _hidden = false;
    private bool _doHidden = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ShowBanner()
    {
        Color newAlpha = gameObject.GetComponent<SpriteRenderer>().color;
        newAlpha.a = 1f;
        gameObject.GetComponent<SpriteRenderer>().color = newAlpha;
        gameObject.GetComponent<AudioSource>().volume = 1f;

        gameObject.SetActive(true);
    }

    public void HideBanner()
    {
        _doHidden = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_doHidden)
        {
            if (_currentTime >= _fadeOutTime)
            {
                if (gameObject.GetComponent<SpriteRenderer>().color.a > 0)
                {
                    Color newAlpha = gameObject.GetComponent<SpriteRenderer>().color;
                    newAlpha.a -= _fadeOutAmount;

                    gameObject.GetComponent<AudioSource>().volume -= _fadeOutAmount;

                    gameObject.GetComponent<SpriteRenderer>().color = newAlpha;
                }
                else
                {
                    gameObject.SetActive(false);
                    _doHidden = false;
                    _hidden = true;
                }
                _currentTime = 0f;
            }
            else
            {
                _currentTime += Time.deltaTime;
            }
        }
    }
}
