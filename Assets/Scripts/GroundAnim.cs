﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundAnim : MonoBehaviour {

    private float _currentOffset = 0.0f;
    private float _actualDistanceTravelled = 0f;
    private Material _groundMaterial;
    private GameObject _outsideGroundGo;
    private Material _outsideMaterial;
    private GameObject _obstaclesGameObject;
    private TypingEngine _gameEngine;
    private bool _doAutomaticGroundAnim = false;
    private float _automaticGroundMovement = 0f;

    private void Awake() {
        _groundMaterial = GetComponent<Renderer>().material;
        _outsideGroundGo = GameObject.Find("outside_bg");
        _outsideMaterial = _outsideGroundGo.GetComponent<Renderer>().material;
        _obstaclesGameObject = GameObject.Find("obstacles");
        _gameEngine = GameObject.Find("game_engine").GetComponent<TypingEngine>();
    }

    public void MoveGround(float amount) {          
        float currentMoveDistance = (amount * Time.deltaTime); // horzRatio;
        _actualDistanceTravelled += currentMoveDistance;
        _currentOffset += currentMoveDistance;

        int hitOffsetCheck = Mathf.FloorToInt(_currentOffset);
        if ((hitOffsetCheck > 0 && hitOffsetCheck % 30 == 0) || hitOffsetCheck > 30)
        {
            _currentOffset = 0f;
        }
        _groundMaterial.SetTextureOffset("_MainTex", new Vector2(_currentOffset, 0f));
        _outsideMaterial.SetTextureOffset("_MainTex", new Vector2(_currentOffset * 0.5f, 0f));
        //_gameEngine.WriteToDebugText("Texture Offset: " + _groundMaterial.GetTextureOffset("_MainTex").ToString());
        
        if (_gameEngine.currentGameModeRules.GameModeHasObstacles)
        {
            if (_obstaclesGameObject.transform.childCount > 0)
            {
                for (int i = 0; i < _obstaclesGameObject.transform.childCount; i++)
                {                    
                    GameObject currentObstacleGameLayer = _obstaclesGameObject.transform.GetChild(i).gameObject;

                    if (currentObstacleGameLayer.transform.childCount > 0)
                    {
                        for (int x = 0; x < currentObstacleGameLayer.transform.childCount; x++)
                        {
                            GameObject currentObstacleGameObject = currentObstacleGameLayer.transform.GetChild(x).gameObject;
                            currentObstacleGameObject.transform.localPosition = new Vector2(currentObstacleGameObject.transform.localPosition.x - currentMoveDistance, _obstaclesGameObject.transform.localPosition.y);
                        }
                    }
                }
            }
        }
        
    }

    //Method added to move the ground in the cinematics but works for everything
    public void CinematicMoveGround(float amount)
    {
        if (amount > 0f || amount < 0f)
        {
            _doAutomaticGroundAnim = true;
            _automaticGroundMovement = amount;
        }
        else
        {
            _doAutomaticGroundAnim = false;
            _automaticGroundMovement = 0f;
        }
        
    }

    public void ResetDistanceTravelled()
    {
        _actualDistanceTravelled = 0f;
    }

    public float GetDistanceTravelled() {
        float result = _actualDistanceTravelled;

        return result;
    }

    private void Update()
    {
        if (_doAutomaticGroundAnim)
        {
            MoveGround(_automaticGroundMovement);
        }
    }
}
