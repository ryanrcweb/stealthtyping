using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditorGameMode : GameMode
{
    public LevelEditorGameMode() : base(false, false, true, false, false, false, true, true, false, false, true, true, false, 99)
    {
        // Everything is enabled
        _loadedText = "";

        _missionTimeCap = -1f;
    }

    private void loadTextIntoString()
    {
        TextAsset tempText = Resources.Load<TextAsset>("the_looking_glass");
        _loadedText = "  " + tempText.text;
    }

    public override string GetGameModeTextContent()
    {
        if (_loadedText.Length == 0)
        {
            loadTextIntoString();
        }

        return _loadedText;
    }

    public override char[] GetTextChars(int startIndex, int length)
    {
        return GetGameModeTextContent().ToUpper().ToCharArray(startIndex, length);
    }

    public override int GetGameModeTextLength()
    {
        if (_loadedText.Length == 0)
        {
            loadTextIntoString();
        }
        return _loadedText.Length;
    }
}
