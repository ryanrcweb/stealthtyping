using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingBarsAnim : MonoBehaviour
{
    public GameObject loadingBarsGo;
    public bool showOnStart = false;

    private int _dirtyCrashStopping = 0;
    private float _timeStep = 0.01f;
    private float _currentTime = 0f;
    private Queue<int> _pastColors = new Queue<int>(3);
    private List<GameObject> _barList = new List<GameObject>();
    private List<Color> _colorList = new List<Color>
    {
        Color.black,
        Color.white,
        new Color32(136, 0, 0, 255),
        new Color32(170, 255, 238, 255),
        new Color32(204, 68, 204, 255),
        new Color32(0, 204, 85, 255),
        new Color32(0, 0, 170, 255),
        new Color32(238, 238, 119, 255),
        new Color32(221, 136, 85, 255),
        new Color32(102, 68, 0, 255),
        new Color32(255, 119, 119, 255),
        new Color32(51, 51, 51, 255),
        new Color32(119, 119, 119, 255),
        new Color32(170, 255, 102, 255),
        new Color32(0, 136, 255, 255),
        new Color32(187, 187, 187, 255)
    };

    // Start is called before the first frame update
    void Start()
    {
        int screenHeightParts = ((Screen.height / 30) + 10);

        for (int x = 0; x < screenHeightParts; x++)
        {
            GameObject currentGo = Instantiate(loadingBarsGo, gameObject.transform);

            currentGo.name = "loading_bar_" + x.ToString();
            currentGo.transform.position = new Vector2(0f, 0f + (float)(((screenHeightParts * 0.3f) / 2) - (x * 0.3f)));            
            _barList.Add(currentGo);
        }

        if (!showOnStart)
        {
            gameObject.SetActive(false);
        }
    }

    public void ShowLoadingBars()
    {
        gameObject.SetActive(true);
    }

    public void HideLoadingBars()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (_currentTime >= _timeStep)
        {
            if (gameObject.activeSelf == true)
            {
                foreach (GameObject bar in _barList)
                {
                    bool colorUsed = true;
                    int nextColor = 0;

                    while (colorUsed && _dirtyCrashStopping < 50)
                    {
                        int tempCol = Random.Range(0, _colorList.Count - 1);
                        // while loop keeps ending up into an endless void so adding 
                        // an emergency exit, will most likely just remove the unique
                        // color check since loading isnt all that long anyway...
                        _dirtyCrashStopping++;

                        if (!_pastColors.Contains(tempCol))
                        {
                            nextColor = tempCol;
                            colorUsed = false;
                        }
                    }

                    if (_pastColors.Count >= 5)
                    {
                        _pastColors.Dequeue();
                    }

                    _pastColors.Enqueue(nextColor);
                    Color currentColor = _colorList[nextColor];

                    bar.GetComponent<SpriteRenderer>().color = currentColor;
                    _dirtyCrashStopping = 0;
                }
            }
            _currentTime = 0f;
        }
        else
        {
            _currentTime += Time.deltaTime;
        }
        
    }
}
