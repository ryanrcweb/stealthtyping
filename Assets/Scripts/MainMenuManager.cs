using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    private void Awake()
    {
        Time.timeScale = 1f;
        if (GameObject.Find("game_globals") == null)
        {
            SceneManager.LoadScene((int) GameModeScenes.Preload);
        }
    }

}
