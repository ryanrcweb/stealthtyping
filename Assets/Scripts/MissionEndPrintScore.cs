using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

public class MissionEndPrintScore : MonoBehaviour
{
    public Text initialScoreText;
    public Text timeText;
    public Text timeBonusText;
    public Text accuracyText;
    public Text accuracyBonusText;
    public Text wpmText;
    public Text wpmBonusText;
    public Text finalScoreText;

    public void UpdateScoreText(uint currentScore, float currentMissionTime, int correctLetters, int incorrectLetters, int totalLettersCount)
    {
        NumberFormatInfo numberFormatting = new NumberFormatInfo();
        numberFormatting.NumberDecimalDigits = 0;
        uint workingScore = currentScore;
        initialScoreText.text = "SCORE: " + workingScore.ToString("N", numberFormatting);

        int timeMins = Mathf.FloorToInt(currentMissionTime / 60f);
        int timeSecs = Mathf.FloorToInt(currentMissionTime % 60f);

        timeText.text = "TIME: " + string.Format("{0:00}:{1:00}", timeMins, timeSecs);
        timeBonusText.text = "NO BONUS";

        int totalTyped = totalLettersCount + incorrectLetters;
        float accuracyFloat = ((float) correctLetters / totalTyped) * 100f;
        int accuracyTotal = Mathf.RoundToInt(accuracyFloat);
        accuracyText.text = "ACCURACY: " + correctLetters.ToString() + " / " + totalTyped.ToString() + " (" + accuracyTotal.ToString() + "%)";

        uint accBonusScore = 500 * ((uint) accuracyTotal / 10);
        if (accBonusScore > 0)
        {
            workingScore += accBonusScore;
            accuracyBonusText.text = "+ " + accBonusScore.ToString("N", numberFormatting);
        }
        else
        {
            accuracyBonusText.text = "NO BONUS";
        }

        float wpmFloat = ((float)correctLetters / 5f) / (float) timeMins;
        int wpm = Mathf.RoundToInt(wpmFloat);
        wpmText.text = wpm.ToString("N", numberFormatting);

        uint wpmBonusScore = 100 * ((uint) wpm);
        if (wpmBonusScore > 0)
        {
            workingScore += wpmBonusScore;
            wpmBonusText.text = "+ " + wpmBonusScore.ToString("N", numberFormatting);
        }
        else
        {
            wpmBonusText.text = "NO BONUS";
        }        

        finalScoreText.text = "FINAL SCORE: " + workingScore.ToString("N", numberFormatting);
    }
}
