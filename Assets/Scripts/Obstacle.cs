﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Obstacle
{
    private int _layer;
    private int _xPosition;
    private int _actualXPosition;
    private int _obstaclePrefabId;

    public Obstacle(ObstacleLayer layer, int xPosition, int obstacleId)
    {
        Layer = layer.layerId;
        XPosition = xPosition;
        ObstaclePrefabId = obstacleId;

        _actualXPosition = XPosition + Mathf.RoundToInt(layer.spawnLocation);
    }

    public int Layer
    {
        get
        {
            return _layer;
        }
        set
        {
            _layer = value;
        }
    }

    public int XPosition
    {
        get
        {
            return _xPosition;
        }
        set
        {
            _xPosition = value;            
        }
    }

    public int ObstaclePrefabId
    {
        get
        {
            return _obstaclePrefabId;
        }
        set
        {
            _obstaclePrefabId = value;
        }
    }

    public int ActualXPosition
    {
        get
        {
            return _actualXPosition;
        }
    }

}
