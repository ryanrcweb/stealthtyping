﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDestroy : MonoBehaviour
{
    // Update is called once per frame
    void Update() {
        if (transform.position.x < -15f){
            Destroy(gameObject);
        }        
    }
}
