using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleLayer : MonoBehaviour
{
    public int layerId;
    public float spawnLocation = 15f;
    public GameObject[] accessableObstaclePrefabs;

    public int GetLayerObstaclePrefabCount()
    {
        return accessableObstaclePrefabs.Length;
    }

    public GameObject GetLayerObstaclePrefab(int index)
    {        
        if (accessableObstaclePrefabs.Length >= index && accessableObstaclePrefabs[index] != null)
        {
            return accessableObstaclePrefabs[index];
        }
        else
        {
            return null;
        }
    }
}
