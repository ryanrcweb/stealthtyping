﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnims : MonoBehaviour
{
    private GameObject _groundGameObject;    
    private Animator _animator;

    private float _slowRunLength = 0.2f;
    private float _animationCurrentTime = 0.0f;

    private float _idleSteppingWaitTime = 0.0f;
    private float _idleSteppingWaitLimit = 1.0f;

    private float _playerCurrentSpeed = 0.0f;
    private float _playerMaxSpeed = 3.0f;

    private int _currentIdleLevel = 0;
    private bool _playerCanIdle;

    //[SerializeField] Sprite player_idle_anim;
    //[SerializeField] Sprite[] player_slow_run_anim;
    //[SerializeField] Sprite[] player_med_run_anim;
    //[SerializeField] Sprite[] player_fast_run_anim;

    public float CurrentSpeed
    {
        get
        {
            return _playerCurrentSpeed;
        }
    }


    private void Awake()
    {
        _groundGameObject = GameObject.Find("ground_bg");
        _animator = GetComponent<Animator>();
        _playerCanIdle = true;
    }

    public void StoppingPlayer()
    {
        _animator.SetBool("isRunning", false);
        _animator.SetBool("isStopping", true);
    }

    public void StopPlayer()
    {
        _animator.SetBool("isRunning", false);
        _animator.SetBool("isStopping", false);
        _animator.SetInteger("runSpeed", 0);
        _playerCurrentSpeed = 0f;
        _playerCanIdle = true;
        //_groundGameObject.GetComponent<GroundAnim>().MoveGround(0f);
    }

    public int IncreaseIdleLevel()
    {
        _idleSteppingWaitTime += Time.deltaTime;        

        if (_idleSteppingWaitTime >= _idleSteppingWaitLimit)
        {
            if (_playerCanIdle)
            {
                _animator.SetBool("isIdle", true);
                // Set the current idle animation
                if (_currentIdleLevel >= 40 && _currentIdleLevel < 45)
                {
                    _animator.SetInteger("idleLevel", 1);                    
                }
                else if (_currentIdleLevel >= 45 && _currentIdleLevel < 55)
                {
                    _animator.SetInteger("idleLevel", 2);                    
                }
                else if (_currentIdleLevel >= 55)
                {
                    _animator.SetInteger("idleLevel", 3);                    
                }
                else
                {
                    _animator.SetInteger("idleLevel", 0);
                }


                if (_currentIdleLevel < 60)
                {
                    _currentIdleLevel++;
                }
            }
            else
            {
                _animator.SetBool("isIdle", false);
                _animator.SetInteger("idleLevel", 0);
                _currentIdleLevel = 0;
            }
            _idleSteppingWaitTime = 0f;
        }

        return _currentIdleLevel;
    }

    public bool IsPlayerAnimIdle()
    {
        bool result = false;

        if (_currentIdleLevel >= 40)
        {
            result = true;
        }

        return result;
    }

    public int GetPlayersCurrentIdleLevel()
    {
        return _currentIdleLevel;
    }

    public int ResetIdleLevel()
    {

        _animator.SetBool("isIdle", false);
        _animator.SetInteger("idleLevel", 0);
        _currentIdleLevel = 0;
        _idleSteppingWaitTime = 0f;
        return _currentIdleLevel;
    }

    public float DoPlayerRun(int actionLevel, bool isStopping)
    {
        _animationCurrentTime += Time.deltaTime;
        if (!isStopping)
        {
            _animator.SetBool("isRunning", true);
            _idleSteppingWaitTime = 0f;
            _playerCanIdle = false;
        }
        else
        {
            StoppingPlayer();
        }

        if (_animationCurrentTime > _slowRunLength)
        {
            switch (actionLevel)
            {
                case 1:
                    _animator.SetInteger("runSpeed", 1);
                    break;
                case 2:
                    _animator.SetInteger("runSpeed", 2);
                    break;
                case 3:
                    _animator.SetInteger("runSpeed", 3);
                    break;
                case 4:
                    _animator.SetInteger("runSpeed", 4);
                    break;
            }

            _animationCurrentTime = 0;
            _playerCurrentSpeed = (0.005f * actionLevel);            
            Debug.Log("player speed: " + _playerCurrentSpeed);
        }

        return CurrentSpeed;
    }

    public void ForcePlayerAnimation(string playerAnimation)
    {
        _animator.SetBool("isRunning", false);
        _animator.SetBool("isStopping", false);
        _animator.SetInteger("runSpeed", 0);
        switch (playerAnimation)
        {
            case "walk":
                _animator.SetBool("isRunning", true);
                _animator.SetInteger("runSpeed", 1);
                break;
            case "run_slow":
                _animator.SetBool("isRunning", true);
                _animator.SetInteger("runSpeed", 2);
                break;
            case "run_mid":
                _animator.SetBool("isRunning", true);
                _animator.SetInteger("runSpeed", 3);
                break;
            case "run_fast":
                _animator.SetBool("isRunning", true);
                _animator.SetInteger("runSpeed", 4);
                break;
            case "stop_slow":
                _animator.SetBool("isRunning", false);
                _animator.SetBool("isStopping", true);
                break;
            case "stop_fast":
                _animator.SetBool("isRunning", false);
                _animator.SetBool("isStopping", true);
                break;
            case "idle":                
            default:
                _animator.SetBool("isRunning", false);
                _animator.SetBool("isStopping", false);
                _animator.SetInteger("runSpeed", 0);
                break;
        }
    }

}
