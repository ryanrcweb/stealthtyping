using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class PreloadManager : MonoBehaviour
{
    public AudioMixer currentAudioMixer;

    private bool _loadStarted = false;

    // Start is called before the first frame update
    void Start()
    {
        // Load the saved audio levels into the mixer
        if (PlayerPrefs.HasKey("currentMasterVol"))
        {
            float currentMasterVol = PlayerPrefs.GetFloat("currentMasterVol");
            float currentMusicVol = PlayerPrefs.GetFloat("currentMusicVol");
            float currentSFXVol = PlayerPrefs.GetFloat("currentSFXVol");

            currentAudioMixer.SetFloat("masterVol", SharedMethods.ConvertToDecibels(currentMasterVol));
            currentAudioMixer.SetFloat("musicVol", SharedMethods.ConvertToDecibels(currentMusicVol));
            currentAudioMixer.SetFloat("sfxVol", SharedMethods.ConvertToDecibels(currentSFXVol));
        }
        else
        {
            float currentMasterVol = 1f;
            float currentMusicVol = 1f;
            float currentSFXVol = 1f;

            currentAudioMixer.SetFloat("masterVol", SharedMethods.ConvertToDecibels(currentMasterVol));
            currentAudioMixer.SetFloat("musicVol", SharedMethods.ConvertToDecibels(currentMusicVol));
            currentAudioMixer.SetFloat("sfxVol", SharedMethods.ConvertToDecibels(currentSFXVol));

            PlayerPrefs.SetFloat("currentMasterVol", currentMasterVol);
            PlayerPrefs.SetFloat("currentMusicVol", currentMusicVol);
            PlayerPrefs.SetFloat("currentSFXVol", currentSFXVol);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!_loadStarted)
        {
            SceneManager.LoadSceneAsync((int) GameModeScenes.MainMenu);
        }
    }
}
