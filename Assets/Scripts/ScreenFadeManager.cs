using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenFadeManager : MonoBehaviour
{    
    private float _fadeStepAmount = 0f;
    private bool _faded = true;
    private bool _fadingIn = false;

    // Start is called before the first frame update
    void Start()
    {
        _fadeStepAmount = 5f;
    }

    public void DoFadeIn()
    {
        SetStartingAlphaLevel(1f);
        _fadingIn = true;
        _faded = false;
    }

    public void DoFadeOut()
    {
        SetStartingAlphaLevel(0f);
        _fadingIn = false;
        _faded = false;
    }

    // Reset the alpha of the GO before starting the fade in/out
    private void SetStartingAlphaLevel(float level)
    {
        Color currentColor = gameObject.GetComponent<SpriteRenderer>().color;
        currentColor.a = level;
        gameObject.GetComponent<SpriteRenderer>().color = currentColor;
    }

    public bool HasFaded()
    {
        return _faded;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_faded)
        {
            float step = _fadeStepAmount * Time.deltaTime;
            Color currentColor = gameObject.GetComponent<SpriteRenderer>().color;
            if (_fadingIn)
            {
                currentColor.a = Mathf.Lerp(currentColor.a, 0f, step);
                if (currentColor.a <= 0f)
                {
                    _faded = true;
                }
            }
            else
            {
                currentColor.a = Mathf.Lerp(currentColor.a, 1f, step);
                if (currentColor.a >= 1f)
                {
                    _faded = true;
                }
            }
            gameObject.GetComponent<SpriteRenderer>().color = currentColor;            
        }
    }
}

