using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SharedMethods
{
    public static float ConvertToDecibels(float value)
    {
        float result = Mathf.Log10(Mathf.Max(value, 0.0001f)) * 20f;
        //float result = (value * 100f) - 80f;

        return result;
    }
}
