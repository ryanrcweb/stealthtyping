using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextColorScroll : MonoBehaviour
{
    private Text _currentText;
    public Gradient strobeColors;
    public float strobeDuration = 3f;

    private float _fauxTimer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        _currentText = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        // Simulate a fake timer since the time scale is set to 0 when
        // the game is paused.
        if (Random.Range(0f, 1000f) >= 900f)
        {
            _fauxTimer += Random.Range(0f, 0.01f);
        }
        float currentTime = Mathf.PingPong((_fauxTimer / strobeDuration), 1f);
        _currentText.color = strobeColors.Evaluate(currentTime);
    }
}
