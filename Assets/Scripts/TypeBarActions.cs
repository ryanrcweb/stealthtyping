﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeBarActions : MonoBehaviour {

    private float _currentOffset = 0.0f;
    private Material _typeBarMaterial;

    private void Awake() {
        _typeBarMaterial = GetComponent<Renderer>().material;
    }

    public void MoveTypeBar(float amount) {
        _currentOffset += amount;        
        _typeBarMaterial.SetTextureOffset("_MainTex", new Vector2(_currentOffset, 0));
    }

    public void MoveBarDanger() {
        _typeBarMaterial.color = new Color(1, 0, 0);
    }

    public void MoveBarWarning() {
        _typeBarMaterial.color = new Color(1, 1, 0);
    }

    public void MoveBarSafe() {
        _typeBarMaterial.color = new Color(0, 1, 0);
    }
}
