﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeBarAmimations : MonoBehaviour
{
    private int _currentAnimationFrame = 0;
    private bool _rev = false;
    private bool _started = false;

    public Sprite[] _animationStateList;

    public void ShowNextFrame()
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = _animationStateList[_currentAnimationFrame];
        if ((_currentAnimationFrame == (_animationStateList.Length - 1)) || ((_currentAnimationFrame == 0) && _started))
        {
            if (_rev)
            {
                _rev = false;
            }
            else
            {
                _rev = true;
            }
        }

        if (_rev)
        {
            _currentAnimationFrame--;
        }
        else
        {
            _currentAnimationFrame++;
        }
        _started = true;
    }
}
