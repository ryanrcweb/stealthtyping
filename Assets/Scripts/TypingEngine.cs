﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SimpleJSON;
using UnityEngine.Playables;
using UnityEngine.Audio;

public class TypingEngine : MonoBehaviour
{
    // Definition Holders for various GameObjects used in this class
    private GameObject _playerGo;
    private PlayerAnims _currentPlayer;
    private GameObject _typingPartsGo;
    private GameObject _typeLeftBarGo;
    private GameObject _typeRightBarGo;
    private GameObject _typeBarBackgroundGo;
    private GameObject _wpmTextGo;
    private GameObject _accTextGo;
    private GameObject _topLetterMarkGo;
    private GameObject _bottomLetterMarkGo;
    private GameObject _scoreTextGo;
    private GameObject _missionTimeTextGo;
    private GameObject _distanceTextGo;
    private GameObject _groundGo;
    private GameObject _obstacleGo;
    private List<GameObject> _obstacleLayerGoList = new List<GameObject>();
    private GameObject _pauseMenuGo;
    private GameObject _goBannerGo;
    private GameObject _comboBonusTextGo;
    private GameObject _cinematicManagerGo;
    private GameObject _cinematicManagerBackgroundGo;
    private GameObject _endingCinematicScoreGo;
    private GameObject _fadeGo;
    private GameObject _skipIntroGo;
    

    //private string _textFileName = "the_looking_glass.txt";
    //private string _textInput;
    private List<char> _currentTextLine = new List<char>();
    private List<char> _bufferText = new List<char>();
    private int _currentLetterIndex = 2;
    private int _currentStringIndex = 0;
    private int _currentPlayerIndex;
    private List<GameObject> _textGoList = new List<GameObject>();

    private Vector3 _typeMainBarLocation;

    private float _cursorFlashLength = 0.4f;
    private float _cursorFlashCurrentTime = 0.0f;

    // Vars for tallying in/correct letters for math
    private int _correctLettersCount = 0;
    private int  _incorrectLettersCount = 0;
    private int _totalLettersCount = 0;

    // Vars for Typing Combos
    private int _currentComboCount = 0;
    private int _bestCombo = 0;
    private int _comboMinStart = 20;
    private uint _comboMultiplier = 50;

    private int _currentTotalLettersCount = 0;
    private float _currentAccuracy = 0f;
    private float _currentActionLevel = 0.0f;
    private float _actionDecayTimer = 0.0f;
    private float _idleTime = 0.0f;
    private bool _endingCinematicEnteredLoop = false;

    private int _currentActionStage = 1;

    // Vars to keep track of if the player has started
    private float _currentTimePassed = 0.0f;
    private bool _hasPlayerStartedTyping = false;

    private int _currentGameState = -1;
    private int _prePauseGameState;
    private float _currentMissionTime = 0.0f;

    private float _currentDistanceTravelled;
    private uint _currentScore = 0;

    private bool _hasIntroFaded = false;
    private bool _isPaused = false;
    private bool _missionEndingSoon = false;
    private bool _missionEndingSoonDone = false;
    private bool _currentCinematicStarted = false;
    private int _missionStartingCinematicState = 0;
    private int _missiongEndingCinematicState = 0;
    private int _cinematicClickSkipStatus = 0;

    // Vars for level data (to be moved into the gamemode class)
    private string _currentLevelJsonData = "{\"obstacles\":{\"0\":{\"prefab_id\":0,\"distance\":1,\"y\":-0.8,\"z\":0,\"spawned\":false},\"1\":{\"prefab_id\":0,\"distance\":5,\"y\":-0.8,\"z\":0,\"spawned\":false},\"2\":{\"prefab_id\":0,\"distance\":6.5,\"y\":-1.8,\"z\":0,\"spawned\":false},\"3\":{\"prefab_id\":0,\"distance\":10,\"y\":10.8,\"z\":0,\"spawned\":false}}}";
    private JSONNode _currentLevelData;
    private string _currentLevelSeed = "nothing";
    private int _currentLevelSeedValue = 0;
    private int _lastDistanceObjectRolled = 0;
    private bool _isPlayerStopping = false;

    // Vars for setting up the stop key and menu key codes
    private KeyCode _stopKeyCode = KeyCode.Return;
    private KeyCode _menuKeyCode = KeyCode.Escape;
    
    [Header("Typing Bar Settings")]
    public int lettersInBatch = 27;
    public Sprite[] fontSpriteList;
    public Sprite[] fontSpecialSpriteList;    
    public Vector2 fontPixelSize = new Vector2(0.4f, 0.65f);
    public float fontPaddingOffset = 0.01f;
    public float typeBarBgTopOffset = 0.1f;

    [Header("Obstacle Prefab Array")]
    //public GameObject[] obstaclePrefabList;
    
    [Header("Player Movement Settings")]
    public float actionDecayRate = 0.1f;
    public float actionCorrectRate = 0.5f;
    public float actionFailedRate = 0.3f;
    public float actionDecayTimeFrame = 0.1f;
    public float actionBreakpointHigh = 60f;
    public float actionBreakpointMedium = 40f;
    public float actionBreakpointLow = 20f;    
    public float idleTimeMaxLimit = 5f;
    public float missionTimeMax = 180.0f;

    [Header("Scene's default Game mode")]
    public int currentGameMode = 1;    
    
    [Header("Main Dialog text GO")]
    public Text mainDialogText;
    public InputField debugText;

    [Header("Audio GO's and Sounds")]
    public AudioMixer currentAudioMixer;
    public BackgroundMusicManager bgMusic;    
    public AudioSource genericSFX;
    public List<AudioClip> genericSFXList = new List<AudioClip>();
    public AnimationClip badLetterAnim;

    // Arrays to hold cinematic playable assets, the background arrays are
    // generally to allow for another to be played whilst it is looping.
    [Header("Cinematic holding arrays")]
    public List<PlayableAsset> missionIntroCinematics = new List<PlayableAsset>();
    public List<PlayableAsset> missionIntroBackgroundCinematics = new List<PlayableAsset>();
    public List<PlayableAsset> missionOutcomeCinematics = new List<PlayableAsset>();
    public List<PlayableAsset> missionOutcomeBackgroundCinematics = new List<PlayableAsset>();
    public List<PlayableAsset> missionEndingCinematics = new List<PlayableAsset>();
    public List<PlayableAsset> missionEndingBackgroundCinematics = new List<PlayableAsset>();

    [HideInInspector]
    public GameMode currentGameModeRules;

    public bool HasPlayerStartedTyping
    {
        get
        {
            return _hasPlayerStartedTyping;
        }
    }

    public uint CurrentScore
    {
        get
        {
            return _currentScore;
        }
    }

    public bool IsGamePaused
    {
        get
        {
            return _isPaused;
        }
    }

    public bool IsPlayerStopping
    {
        get
        {
            return _isPlayerStopping;
        }
    }

    public string CurrentLevelSeed
    {
        get
        {
            return _currentLevelSeed;
        }
        set
        {
            _currentLevelSeed = value;
        }
    }

    public int CurrentLevelSeedValue
    {
        get
        {
            if (_currentLevelSeedValue == 0)
            {
                string working_seed = "";
                byte[] seed_ascii_vals = Encoding.ASCII.GetBytes(CurrentLevelSeed);
                foreach (byte c in seed_ascii_vals)
                {
                    working_seed += c.ToString();
                }

                if (!int.TryParse(working_seed, out _currentLevelSeedValue))
                {
                    _currentLevelSeedValue = 1100110;
                }
            }            

            return _currentLevelSeedValue;
        }
    }

    public InputField DebugText
    {
        get
        {
            return debugText;
        }
    }

    public void WriteToDebugText(string newLine)
    {
        if (DebugText != null && DebugText.gameObject.activeSelf == true)
        {
            //DebugText.text += System.DateTime.Now.ToString("HH:mm:ss") + " - " + newLine + "\n";
        }
    }

    // Start is called before the first frame update
    private void Awake()
    {
        if (GameObject.Find("game_globals") == null)
        {
            SceneManager.LoadScene((int)GameModeScenes.Preload);
            return;
        }

        WriteToDebugText("Screen Resolution: " + Screen.width.ToString() + " by " + Screen.height.ToString());

        _currentLevelSeed = GameGlobals.Instance.currentSeed;
        Time.timeScale = 1;
        Random.InitState(CurrentLevelSeedValue);
        
        if (GameGlobals.Instance.overrideMissionGameMode)
        {
            currentGameMode = GameGlobals.Instance.overrideMissionGameModeValue;
        }

        switch (currentGameMode)
        {
            case 1:
                currentGameModeRules = new DevGameMode();
                break;
            case 998:
                currentGameModeRules = new LevelEditorGameMode();
                break;
            case 999:
                currentGameModeRules = new AnimTestGameMode();
                break;
        }

        //_currentLevelData = JSON.Parse(_currentLevelJsonData);
        _currentGameState = currentGameModeRules.InitGameState;

        _fadeGo = GameObject.Find("screen_fade");        
        if (currentGameModeRules.GameModeHasPlayer)
        {
            _playerGo = GameObject.Find("player");
            _currentPlayer = _playerGo.GetComponent<PlayerAnims>();            
        }
        if (currentGameModeRules.GameModeHasWPM)
        {
            _wpmTextGo = GameObject.Find("wpm_text");
        }
        if (currentGameModeRules.GameModeHasAccuracy)
        {
            _accTextGo = GameObject.Find("acc_text");
        }
        if (currentGameModeRules.GameModeHasDistance)
        {
            _distanceTextGo = GameObject.Find("distance_text");
        }
        if (currentGameModeRules.GameModeHasScore)
        {
            _scoreTextGo = GameObject.Find("score_text");
            _comboBonusTextGo = GameObject.Find("combo_bonus_text");
        }
        if (currentGameModeRules.GameModeHasTimer)
        {
            if (GameGlobals.Instance.overrideMissionTime)
            {
                missionTimeMax = GameGlobals.Instance.overrideMissionTimeValue;
            }
            else
            {
                missionTimeMax = currentGameModeRules.MissionTimeCap;
            }            

            _missionTimeTextGo = GameObject.Find("mission_time_text");
            _goBannerGo = GameObject.Find("start_go_banner");
        }
        if (currentGameModeRules.GameModeHasGround)
        {
            _groundGo = GameObject.Find("ground_bg");
        }
        if (currentGameModeRules.GameModeHasObstacles)
        {
            _obstacleGo = GameObject.Find("obstacles");
            int obstacleChildCount = _obstacleGo.transform.childCount;
            if (obstacleChildCount > 0)
            {
                for (int i = 0; i < obstacleChildCount; i++)
                {
                    GameObject currentLayerObject = GameObject.Find("obstacles/layer_" + i.ToString());
                    if (currentLayerObject != null)
                    {
                        _obstacleLayerGoList.Add(currentLayerObject);
                    }                    
                }
            }
        }
        if (currentGameModeRules.GameModeHasPauseMenu)
        {
            _pauseMenuGo = GameObject.Find("pause_menu");
            _pauseMenuGo.SetActive(false);
        }
        if (currentGameModeRules.GameModeHasCinematics)
        {            
            _cinematicManagerGo = GameObject.Find("cinematics_manager");
            _cinematicManagerBackgroundGo = GameObject.Find("cinematics_manager_background");

            _endingCinematicScoreGo = GameObject.Find("timeline_manager_ending_score");            
            _endingCinematicScoreGo.SetActive(false);

            _skipIntroGo = GameObject.Find("skip_cinematic_text_canvas");
            if (_skipIntroGo != null)
            {
                _skipIntroGo.SetActive(false);
            }
        }

        if (currentGameModeRules.GameModeHasLetters)
        {
            _totalLettersCount = currentGameModeRules.GetGameModeTextLength();
            _typingPartsGo = GameObject.Find("type_parts");
            _typeLeftBarGo = GameObject.Find("type_left");
            _typeRightBarGo = GameObject.Find("type_right");
            _typeBarBackgroundGo = GameObject.Find("type_main_bg");

            _typeMainBarLocation = new Vector3(_typeBarBackgroundGo.transform.position.x, _typeBarBackgroundGo.transform.position.y);
            _currentTextLine.AddRange(currentGameModeRules.GetTextChars(_currentStringIndex, lettersInBatch));
            _currentStringIndex += lettersInBatch;

            _bufferText.AddRange(currentGameModeRules.GetTextChars(_currentStringIndex, lettersInBatch));
            _currentStringIndex += lettersInBatch;

            _currentPlayerIndex = _currentLetterIndex;

            int setupCount = 0;
            foreach (char currentCharacter in _currentTextLine)
            {
                GameObject currentGameObject = new GameObject("current_letter_" + setupCount.ToString());
                currentGameObject.transform.parent = _typingPartsGo.transform.Find("letters");

                SpriteRenderer currentSpriteRenderer = new SpriteRenderer();
                currentSpriteRenderer = currentGameObject.AddComponent<SpriteRenderer>();

                int[] textureIndex = GetFontTextureIndex(currentCharacter);

                if (textureIndex[0] != -1)
                {
                    float xOffset = (_typeBarBackgroundGo.GetComponent<SpriteRenderer>().bounds.min.x + ((fontPixelSize.x) / 2)) + ((fontPixelSize.x + fontPaddingOffset) * setupCount);
                    currentGameObject.transform.position = new Vector3(xOffset, (_typeMainBarLocation.y - typeBarBgTopOffset), -5f);
                    switch (textureIndex[0])
                    {
                        case 1:
                            currentSpriteRenderer.sprite = fontSpriteList[textureIndex[1]];
                            break;
                        case 2:
                            currentSpriteRenderer.sprite = fontSpecialSpriteList[textureIndex[1]];
                            break;
                    }
                    if (setupCount == _currentLetterIndex)
                    {
                        currentSpriteRenderer.color = new Color(1, 1, 0);
                    }
                    else if (setupCount > _currentLetterIndex)
                    {
                        currentSpriteRenderer.color = new Color(1, 0, 0);
                    }
                    else
                    {
                        currentSpriteRenderer.color = new Color(0, 1, 0, 0);
                    }

                    currentSpriteRenderer.transform.localScale = fontPixelSize;
                }

                Animation currentAnimation = currentGameObject.AddComponent<Animation>();
                currentAnimation.playAutomatically = false;
                currentAnimation.clip = badLetterAnim;

                _textGoList.Add(currentGameObject);
                setupCount++;
            }

            if (currentGameModeRules.GameModeHasCharacterHighlighter)
            {
                _topLetterMarkGo = GameObject.Find("top_letter_mark");
                _bottomLetterMarkGo = GameObject.Find("bottom_letter_mark");
                MoveLetterMarkers();
            }            
        }

        // Allow special modes to override any init settings if needed
        // Mainly for adding special cinematics for easter eggs
        currentGameModeRules.MakeGameModeChangesToEngineInit(this);
    }

    public int[] GetFontTextureIndex(char letter)
    {
        byte[] currentCharacterByte = System.Text.Encoding.ASCII.GetBytes(letter.ToString());

        int[] textureIndex = { -1, 0 };
        if (currentCharacterByte[0] >= 48 && currentCharacterByte[0] <= 57)
        {
            textureIndex[0] = 1;
            textureIndex[1] = currentCharacterByte[0] - 48;
        }
        else if (currentCharacterByte[0] == 32)
        {
            textureIndex[0] = 1;
            textureIndex[1] = 38;
        }
        else if (currentCharacterByte[0] >= 65 && currentCharacterByte[0] <= 90)
        {
            textureIndex[0] = 1;
            textureIndex[1] = currentCharacterByte[0] - 55;
        }
        else
        {
            switch (currentCharacterByte[0])
            {
                case 33:
                    textureIndex[0] = 2;
                    textureIndex[1] = 0;
                    break;
                case 34:
                    textureIndex[0] = 2;
                    textureIndex[1] = 1;
                    break;
                case 39:
                    textureIndex[0] = 2;
                    textureIndex[1] = 2;
                    break;
                case 44:
                    textureIndex[0] = 2;
                    textureIndex[1] = 3;
                    break;
                case 46:
                    textureIndex[0] = 2;
                    textureIndex[1] = 4;
                    break;
                case 40:
                    textureIndex[0] = 2;
                    textureIndex[1] = 5;
                    break;
                case 41:
                    textureIndex[0] = 2;
                    textureIndex[1] = 6;
                    break;
                case 58:
                    textureIndex[0] = 2;
                    textureIndex[1] = 7;
                    break;
                case 59:
                    textureIndex[0] = 2;
                    textureIndex[1] = 8;
                    break;
                case 63:
                    textureIndex[0] = 2;
                    textureIndex[1] = 9;
                    break;
                case 45:
                    textureIndex[0] = 2;
                    textureIndex[1] = 10;
                    break;
            }
        }

        return textureIndex;
    }

    public void AddScore(uint amount)
    {
        if (currentGameModeRules.GameModeHasScore)
        {
            _currentScore += amount;
            _scoreTextGo.GetComponent<Text>().text = "SCORE: " + _currentScore.ToString().PadLeft(10, '0');
        }
    }

    public void UpdateMissionTime()
    {
        if (currentGameModeRules.GameModeHasTimer)
        {
            int timeMins = Mathf.FloorToInt(_currentMissionTime / 60f);
            int timeSecs = Mathf.FloorToInt(_currentMissionTime % 60f);

            _missionTimeTextGo.GetComponent<Text>().text = "TIME: " + string.Format("{0:00}:{1:00}", timeMins, timeSecs);
        }
    }

    public void ChangeCurrentGameState(int newState)
    {
        int oldState = _currentGameState;
        _currentGameState = newState;
        Debug.Log("Changed state from " + oldState.ToString() + " to " + newState.ToString());
    }

    private void TypeLetter(char letter)
    {
        if (currentGameModeRules.GameModeHasLetters)
        {
            if (letter == _currentTextLine[_currentLetterIndex])
            {
                if (_bufferText.Count == 0)
                {
                    int nextBatchIndex = (_currentStringIndex + (lettersInBatch - 1));
                    int remainerDiff = currentGameModeRules.GetGameModeTextLength() - nextBatchIndex;
                    int blankCharactersToAdd = 0;
                    if (remainerDiff < 0)
                    {
                        blankCharactersToAdd = Mathf.Abs(remainerDiff);
                    }

                    if (nextBatchIndex > currentGameModeRules.GetGameModeTextLength())
                    {
                        if (blankCharactersToAdd < lettersInBatch)
                        {
                            _bufferText.AddRange(currentGameModeRules.GetTextChars(_currentStringIndex, (currentGameModeRules.GetGameModeTextLength() - _currentStringIndex)));
                            _currentStringIndex += lettersInBatch;
                        }
                    }
                    else
                    {
                        _bufferText.AddRange(currentGameModeRules.GetTextChars(_currentStringIndex, lettersInBatch));
                        _currentStringIndex += lettersInBatch;
                    }
                    if (remainerDiff < 0)
                    {
                        for (int i = 0; i < blankCharactersToAdd; i++)
                        {
                            _bufferText.Add(' ');
                        }
                    }
                }

                _currentTextLine.RemoveAt(0);
                _currentTextLine.Add(_bufferText[0]);
                _bufferText.RemoveAt(0);

                int currentLetterCount = 0;
                foreach (GameObject currentLetterGameObject in _textGoList)
                {
                    int[] textureIndex = GetFontTextureIndex(_currentTextLine[currentLetterCount]);
                    switch (textureIndex[0])
                    {
                        case 1:
                            currentLetterGameObject.GetComponent<SpriteRenderer>().sprite = fontSpriteList[textureIndex[1]];
                            break;
                        case 2:
                            currentLetterGameObject.GetComponent<SpriteRenderer>().sprite = fontSpecialSpriteList[textureIndex[1]];
                            break;
                    }
                    currentLetterCount++;
                }

                _typeLeftBarGo.GetComponent<TypeLeftActions>().ShowNextFrame();
                _typeRightBarGo.GetComponent<TypeRightActions>().ShowNextFrame();
                _typeBarBackgroundGo.GetComponent<TypeBarActions>().MoveTypeBar(fontPixelSize.x);
                _currentComboCount++;
                MoveLetterMarkers();
                if (_currentActionLevel < actionBreakpointHigh)
                {
                    _currentActionLevel += actionCorrectRate;
                }

                if (currentGameModeRules.GameModeHasScore)
                {
                    AddScore((uint)((1 * _currentActionStage)));
                    _correctLettersCount++;
                    _currentTotalLettersCount++;
                    if (_currentComboCount >= _comboMinStart)
                    {
                        _comboBonusTextGo.GetComponent<ComboTextManager>().UpdateComboText(_currentComboCount, false);
                    }
                }
            }
            else
            {                
                if (_currentActionLevel > 0)
                {
                    _currentActionLevel -= actionFailedRate;
                }
                if (_currentComboCount > _bestCombo)
                {
                    _bestCombo = _currentComboCount;
                }

                if (currentGameModeRules.GameModeHasScore)
                {
                    if (_currentComboCount >= _comboMinStart)
                    {
                        _comboBonusTextGo.GetComponent<ComboTextManager>().UpdateComboText(_currentComboCount, true, GetCurrentComboScoreTotal());
                        AddScore(GetCurrentComboScoreTotal());
                    }
                }
                _currentComboCount = 0;
                _incorrectLettersCount++;
                _textGoList[_currentLetterIndex].GetComponent<Animation>().Play();
                if (genericSFXList[0] != null)
                {
                    genericSFX.PlayOneShot(genericSFXList[0]);
                }
            }
        }
    }

    public uint GetCurrentComboScoreTotal()
    {
        uint result = 0;

        if (_currentComboCount > _comboMinStart)
        {
            result = (uint)_currentComboCount * _comboMultiplier;
        }

        return result;
    }

    public void MoveLetterMarkers()
    {
        if (currentGameModeRules.GameModeHasCharacterHighlighter)
        {
            _topLetterMarkGo.transform.position = new Vector3(_textGoList[_currentPlayerIndex].transform.position.x, _topLetterMarkGo.transform.position.y, _topLetterMarkGo.transform.position.z);
            _bottomLetterMarkGo.transform.position = new Vector3(_textGoList[_currentPlayerIndex].transform.position.x, _bottomLetterMarkGo.transform.position.y, _topLetterMarkGo.transform.position.z);
        }
    }

    public void FlashCursor()
    {
        if (currentGameModeRules.GameModeHasLetters)
        {
            _cursorFlashCurrentTime += Time.deltaTime;
            if (_cursorFlashCurrentTime > _cursorFlashLength)
            {
                if (_textGoList[_currentLetterIndex].GetComponent<SpriteRenderer>().color.r == 1)
                {
                    _textGoList[_currentLetterIndex].GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0);
                    if (currentGameModeRules.GameModeHasCharacterHighlighter)
                    {
                        _topLetterMarkGo.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0);
                        _bottomLetterMarkGo.GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0);
                    }
                }
                else
                {
                    _textGoList[_currentLetterIndex].GetComponent<SpriteRenderer>().color = new Color(1, 1, 0);
                    if (currentGameModeRules.GameModeHasCharacterHighlighter)
                    {
                        _topLetterMarkGo.GetComponent<SpriteRenderer>().color = new Color(1, 1, 0);
                        _bottomLetterMarkGo.GetComponent<SpriteRenderer>().color = new Color(1, 1, 0);
                    }
                }
                _cursorFlashCurrentTime = 0.0f;
            }
        }
    }

    public void RestartCurrentGame()
    {
        _fadeGo.GetComponent<ScreenFadeManager>().DoFadeOut();
        bgMusic.StopCurrentAudioForGood();
        if (Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }
        GameGlobals.Instance.LoadSelectedLevel((GameModeScenes)SceneManager.GetActiveScene().buildIndex);        
    }

    public void UnpauseGame()
    {
        _pauseMenuGo.GetComponent<ButtonScripts>().CloseOptionsPanel();
        _pauseMenuGo.SetActive(false);
        _isPaused = false;
        AudioListener.pause = false;
        Time.timeScale = 1f;
        if (_prePauseGameState != 0)
        {
            _currentGameState = _prePauseGameState;
        }
        else
        {
            _currentGameState = 1;
        }
        _prePauseGameState = 0;
    }

    public void UpdateDialogTextBox(string newText)
    {
        mainDialogText.text = newText;
    }

    public void StartGameAfterCutscene()
    {        
        _currentGameState = 1;        
    }

    public void  EndCinematicEndingLoopStarted()
    {
        _endingCinematicEnteredLoop = true;
    }

    public void EndCinematicEndingSoonCutscene()
    {
        _missionEndingSoonDone = true;
    }

    public void AdvanceEndingCinematic()
    {
        _currentCinematicStarted = false;
        _missiongEndingCinematicState++;
    }

    public void AdvanceIntroCinematic()
    {
        _currentCinematicStarted = false;
        _missionStartingCinematicState++;
    }

    // Update is called once per frame
    private void Update()
    {
        if (_fadeGo != null)
        {
            if (!_hasIntroFaded)
            {
                _fadeGo.GetComponent<ScreenFadeManager>().DoFadeIn();
                _hasIntroFaded = true;
            }
        }


        bool stoppingInit = false;

        if (Input.GetKeyDown(_menuKeyCode))
        {
            if (currentGameModeRules.GameModeHasPauseMenu)
            {
                Debug.Log("Escape down");
                Debug.Log("_isPaused - " + IsGamePaused.ToString());
                if (Time.timeScale == 0f)
                {
                    UnpauseGame();
                }
                else
                {
                    _pauseMenuGo.SetActive(true);
                    AudioListener.pause = true;
                    _isPaused = true;
                    Time.timeScale = 0f;
                    _prePauseGameState = _currentGameState;
                    _currentGameState = 0;
                }
            }
        }

        switch (_currentGameState)
        {
            // Game Starting/Cinematics
            case -1:
                if (_missionStartingCinematicState < missionIntroCinematics.Count)
                {
                    if (!_currentCinematicStarted)
                    {
                        _cinematicManagerGo.GetComponent<PlayableDirector>().Play(missionIntroCinematics[_missionStartingCinematicState]);
                        _currentCinematicStarted = true;
                    }
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (_cinematicClickSkipStatus < 1)
                        {
                            if (_skipIntroGo != null)
                            {
                                _skipIntroGo.SetActive(true);
                                _skipIntroGo.GetComponentInChildren<Text>().text = "Click to skip";
                            }
                            _cinematicClickSkipStatus++;
                        }
                        else
                        {
                            if (_skipIntroGo != null)
                            {
                                _skipIntroGo.GetComponentInChildren<Text>().text = "Skipping...";
                            }
                            Time.timeScale = 10f;
                        }
                    }
                }
                else
                {
                    if (_skipIntroGo != null)
                    {
                        _skipIntroGo.SetActive(false);                        
                    }
                    _cinematicManagerGo.GetComponent<PlayableDirector>().Stop();
                    _cinematicManagerGo.GetComponent<PlayableDirector>().playableAsset = null;
                    _currentGameState = 1;
                    Time.timeScale = 1f;
                }
                break;
            // Game Paused
            case 0:
                break;
            // Main Game Loop
            case 1:
                if (Input.GetKeyDown(_stopKeyCode))
                {
                    // stopping key for campaign mode, causes player to enter stopping mode
                    // when they have actually started the game
                    if (_hasPlayerStartedTyping)
                    {
                        stoppingInit = true;
                        _isPlayerStopping = true;
                    }
                }
                else
                {
                    if (Input.inputString.Length > 0)
                    {
                        foreach (char letter in Input.inputString)
                        {
                            TypeLetter(char.ToUpper(letter));

                            // checks if the player has started yet, if so removes
                            // the go banner and starts the actual level music
                            if (!_hasPlayerStartedTyping)
                            {
                                bgMusic.QueueNextAudio(1);
                            }
                            _hasPlayerStartedTyping = true;
                            if (currentGameModeRules.GameModeHasTimer && _goBannerGo.activeSelf)
                            {
                                _goBannerGo.GetComponent<GoBannerAnims>().HideBanner();
                            }
                        }
                        _idleTime = 0.0f;
                    }
                    else
                    {

                    }
                }

                // Flash the current letter
                FlashCursor();

                // Wait til the player has actually started typing before checking for anything.
                if (HasPlayerStartedTyping)
                {
                    _currentTimePassed += Time.deltaTime;
                    UpdateMissionTime();

                    if (currentGameModeRules.GameModeHasPlayer)
                    {
                        if (!stoppingInit)
                        {
                            // If the player is "stopped" and the idle timer has passed the max value, 
                            // start playing the idle animations and show go banner
                            if (_idleTime > idleTimeMaxLimit)
                            {
                                int currentPlayerIdleLevel = _currentPlayer.IncreaseIdleLevel();
                                _currentActionLevel = 0f;
                                _isPlayerStopping = true;    
                                if (currentPlayerIdleLevel >= 50)
                                {
                                    _goBannerGo.GetComponent<GoBannerAnims>().ShowBanner();
                                }
                                if (currentGameModeRules.GameModeHasScore)
                                {
                                    if (_currentComboCount >= _comboMinStart)
                                    {
                                        _comboBonusTextGo.GetComponent<ComboTextManager>().UpdateComboText(_currentComboCount, true, GetCurrentComboScoreTotal());
                                        AddScore(GetCurrentComboScoreTotal());
                                        _currentComboCount = 0;
                                    }
                                }
                            }
                            // If current idle timer tick is almost to max start to stop the player run animation
                            else if (_idleTime > idleTimeMaxLimit * 0.8f)
                            {
                                _isPlayerStopping = true;
                            }
                            else
                            {                                
                                _isPlayerStopping = false;                                
                            }                        
                        }

                        // Player run animations and ground movement speeds are determined
                        // by preset action break points, low is broken down into 2 sub points 
                        // for walking (barely typing) and slow running
                        if (_currentActionLevel <= actionBreakpointLow)
                        {
                            _typeBarBackgroundGo.GetComponent<TypeBarActions>().MoveBarDanger();
                            if (_currentActionLevel <= (actionBreakpointLow * 0.3f))
                            {
                                _currentActionStage = 1;
                            }
                            else
                            {
                                _currentActionStage = 2;
                            }
                        }
                        else if (_currentActionLevel <= actionBreakpointMedium)
                        {
                            _typeBarBackgroundGo.GetComponent<TypeBarActions>().MoveBarWarning();
                            _currentActionStage = 3;
                        }
                        else if (_currentActionLevel <= actionBreakpointHigh)
                        {
                            _typeBarBackgroundGo.GetComponent<TypeBarActions>().MoveBarSafe();
                            _currentActionStage = 4;
                        }

                        // if the current action level is 0 (or below),
                        // stop the player animation and ground movement
                        if (_currentActionLevel <= 0f)
                        {
                            _currentPlayer.StopPlayer();
                            _currentActionLevel = 0.0f;
                            if (_currentPlayer.CurrentSpeed <= 0f)
                            {
                                _groundGo.GetComponent<GroundAnim>().CinematicMoveGround(0);
                            }                            
                            stoppingInit = false;                            
                        }
                        else
                        {
                            // Player is typing, reset the idle level and timer, also remove the go banner if active
                            _currentPlayer.ResetIdleLevel();
                            _idleTime = 0f;
                            if (currentGameModeRules.GameModeHasTimer && _goBannerGo.activeSelf)
                            {
                                _goBannerGo.GetComponent<GoBannerAnims>().HideBanner();
                            }

                            // Move the ground offset by the current players speed
                            float playerCurrentSpeed = _currentPlayer.DoPlayerRun(_currentActionStage, IsPlayerStopping);
                            if (currentGameModeRules.GameModeHasGround)
                            {
                                _groundGo.GetComponent<GroundAnim>().CinematicMoveGround(currentGameModeRules.GetGroundSpeedFromPlayerSpeed(_currentActionStage));
                            }

                            // start action level decay so the player slows down if they cant keep up the wpm
                            if (_actionDecayTimer > actionDecayTimeFrame)
                            {
                                if (_currentActionLevel > 0)
                                {
                                    _currentActionLevel -= actionDecayRate;
                                }
                                _actionDecayTimer = 0.0f;
                            }
                            _actionDecayTimer += Time.deltaTime;
                        }

                    }

                    if (currentGameModeRules.GameModeHasObstacles)
                    {
                        // Old obstacle generation code would get predefined from a json object
                        //foreach (JSONNode current_node in _currentLevelData["obstacles"]) {
                        //    if (!current_node["spawned"].AsBool) {
                        //        if (_currentDistanceTravelled >= current_node["distance"].AsFloat) {
                        //            GameObject currentObstacleGameObject = Instantiate(_obstaclePrefabList[current_node["prefab_id"].AsInt], _obstacleGo.transform);
                        //            currentObstacleGameObject.transform.position = new Vector3(15, current_node["y"].AsFloat, current_node["z"].AsFloat);
                        //            current_node["spawned"] = true;
                        //        }
                        //    }
                        //}

                        // Obstacles current randomly generated on the fly, this code to be moved to the GameMode class
                        int distanceInt = Mathf.FloorToInt(_groundGo.GetComponent<GroundAnim>().GetDistanceTravelled());
                        if ((distanceInt >= 1 && (distanceInt % 5) == 0) && (_lastDistanceObjectRolled < distanceInt))
                        {
                            foreach (GameObject currentLayerGo in _obstacleLayerGoList)
                            {
                                if (currentLayerGo.GetComponent<ObstacleLayer>().GetLayerObstaclePrefabCount() > 0)
                                {
                                    int generateObstacleCheck = Random.Range(0, 100);
                                    if (generateObstacleCheck >= 50)
                                    {
                                        int selectedPrefabNumber = Random.Range(0, (currentLayerGo.GetComponent<ObstacleLayer>().GetLayerObstaclePrefabCount() - 1));
                                        GameObject currentObstacleGameObject = Instantiate(currentLayerGo.GetComponent<ObstacleLayer>().GetLayerObstaclePrefab(selectedPrefabNumber), currentLayerGo.transform, false);
                                        //currentObstacleGameObject.transform.parent = currentLayerGo.transform;
                                        currentObstacleGameObject.transform.localPosition = new Vector2(currentLayerGo.GetComponent<ObstacleLayer>().spawnLocation, 0f);
                                    }
                                }
                            }
                            _lastDistanceObjectRolled = distanceInt;
                        }
                    }

                    // If player is stopping decay action level faster
                    if (stoppingInit)
                    {
                        _currentActionLevel *= 0.2f;
                    }

                    _distanceTextGo.GetComponent<Text>().text = "X: " + Mathf.FloorToInt(_groundGo.GetComponent<GroundAnim>().GetDistanceTravelled());

                    _idleTime += Time.deltaTime;
                    // Checks to see if the mission timer is almost hit, when close play the count down timeline
                    if (_currentMissionTime < (missionTimeMax - 17))
                    {
                        _currentMissionTime += Time.deltaTime;
                    }
                    else if (_currentMissionTime >= (missionTimeMax - 17) && _currentMissionTime < missionTimeMax)
                    {
                        _currentMissionTime += Time.deltaTime;
                        if (currentGameModeRules.GameModeHasCinematics)
                        {
                            if (!_missionEndingSoon)
                            {
                                _cinematicManagerGo.GetComponent<PlayableDirector>().Play(missionEndingCinematics[_missiongEndingCinematicState]);
                                _missionEndingSoon = true;
                                _missionEndingSoonDone = false;
                            }
                        }
                    }
                    else if (_currentMissionTime >= missionTimeMax)
                    {
                        if (_missionEndingSoonDone)
                        {
                            if (currentGameModeRules.GameModeHasScore)
                            {
                                if (_currentComboCount >= _comboMinStart)
                                {
                                    _comboBonusTextGo.GetComponent<ComboTextManager>().UpdateComboText(_currentComboCount, true, GetCurrentComboScoreTotal());
                                    AddScore(GetCurrentComboScoreTotal());
                                    _currentComboCount = 0;
                                }
                            }
                            _currentMissionTime = missionTimeMax;
                            if (currentGameModeRules.GameModeHasCinematics)
                            {
                                if (currentGameModeRules.GameModeHasTimer && _goBannerGo.activeSelf)
                                {
                                    _goBannerGo.GetComponent<GoBannerAnims>().HideBanner();
                                }
                                int outcomeValue = 0;
                                if (_currentPlayer.IncreaseIdleLevel() > 50)
                                {
                                    outcomeValue = 1;
                                }
                                _cinematicManagerGo.GetComponent<PlayableDirector>().Play(missionOutcomeCinematics[outcomeValue]);                                
                                _currentGameState = 2;
                            }
                        }
                    }
                    //Debug.Log(_currentActionLevel);
                }
                break;
            // Game End state and Cinematics
            case 2:
                switch (_missiongEndingCinematicState)
                {
                    case 1:
                        if (!_currentCinematicStarted)
                        {
                            _cinematicManagerGo.GetComponent<PlayableDirector>().Play(missionEndingCinematics[_missiongEndingCinematicState]);
                            _currentCinematicStarted = true;
                        }
                        break;
                    case 2:
                        if (_endingCinematicEnteredLoop)
                        {
                            _endingCinematicScoreGo.SetActive(true);
                            _endingCinematicScoreGo.GetComponent<MissionEndPrintScore>().UpdateScoreText(_currentScore, _currentMissionTime, _correctLettersCount, _incorrectLettersCount, _currentTotalLettersCount);
                            _endingCinematicEnteredLoop = false;
                        }
                        break;
                }                
                break;

            case 99:
                float currentMoveSpeed = 0f;
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    currentMoveSpeed = 5f;
                }
                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    currentMoveSpeed = -5f;
                }

                if (currentGameModeRules.GameModeHasGround)
                {
                    _groundGo.GetComponent<GroundAnim>().CinematicMoveGround(currentMoveSpeed);
                    _distanceTextGo.GetComponent<Text>().text = "X: " + Mathf.FloorToInt(_groundGo.GetComponent<GroundAnim>().GetDistanceTravelled());
                }

                break;
        }
    }
}
